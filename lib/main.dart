import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:bootcamp/travel/api.dart';
import 'package:bootcamp/travel/travel_model.dart';
import 'package:bootcamp/travel/travel_place.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.green,
      ),
      home: const MyHomePage(title: 'Bootcamp'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TravelModel? places = TravelModel(places: []);

  @override
  void initState() {
    // getHttp('https://run.mocky.io/v3/7e2382a9-6cfd-4850-bda9-b6d1fa7295e9')
    getHttp('https://run.mocky.io/v3/fbbdae10-8e6a-4ba3-ab1c-af82479b8d05')
        .then((value) {
      //value = places

      debugPrint('size = ${places?.places?.length ?? 0}');
      setState(() {
        places = value;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (places == null) {
      return Container(
          color: Colors.white,
          child: const Align(
              alignment: Alignment.center,
              child: Text(
                'something went wrong',
                style: TextStyle(color: Colors.blue, fontSize: 18),
              )));
    } else if (places?.places?.isNotEmpty ?? false) {
      return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
        ),
        body: TravelPlace(
          travelList: places!,
        ),
      );
    } else {
      return Container(
          color: Colors.white,
          child: const Align(
              alignment: Alignment.center,
              child: Text(
                'loading..',
                style: TextStyle(fontSize: 18,color: Colors.grey),
              )));
    }
  }
}

import 'package:bootcamp/travel/travel_model.dart';
import 'package:bootcamp/travel/travel_place.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

Future<TravelModel?> getHttp(String url) async {
  try {
    var response = await Dio().get(url);
    debugPrint('response = ${response.data}');
    return TravelModel.fromJson(response.data);
  } catch (e) {
    debugPrint(e.toString());
  }
  return null;
}

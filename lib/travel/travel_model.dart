import 'package:flutter/foundation.dart';

/// places : [{"title":"places1","description":"description1","rate":3,"image":"https://www.planetware.com/wpimages/2020/02/france-in-pictures-beautiful-places-to-photograph-eiffel-tower.jpg"},{"title":"places2","description":"description2","rate":4,"image":"https://image.shutterstock.com/image-photo/kerala-most-beautiful-place-india-600w-1993000271.jpg"},{"title":"places3","description":"description3","rate":4,"image":"https://image.shutterstock.com/image-photo/kerala-most-beautiful-place-india-600w-1993000271.jpg"}]

class TravelModel {
  TravelModel({
      List<Places>? places,}){
    _places = places;
}

  TravelModel.fromJson(dynamic json) {
    if (json['places'] != null) {
      _places = [];
      json['places'].forEach((v) {
        _places?.add(Places.fromJson(v));
      });
    }else{
      debugPrint("json['places'] == null");
    }
  }
  List<Places>? _places;
TravelModel copyWith({  List<Places>? places,
}) => TravelModel(  places: places ?? _places,
);
  List<Places>? get places => _places;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_places != null) {
      map['places'] = _places?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// title : "places1"
/// description : "description1"
/// rate : 3
/// image : "https://www.planetware.com/wpimages/2020/02/france-in-pictures-beautiful-places-to-photograph-eiffel-tower.jpg"

class Places {
  Places({
      String? title, 
      String? description, 
      int? rate, 
      String? image,}){
    _title = title;
    _description = description;
    _rate = rate;
    _image = image;
}

  Places.fromJson(dynamic json) {
    _title = json['title'];
    _description = json['description'];
    _rate = json['rate'];
    _image = json['image'];
  }
  String? _title;
  String? _description;
  int? _rate;
  String? _image;
Places copyWith({  String? title,
  String? description,
  int? rate,
  String? image,
}) => Places(  title: title ?? _title,
  description: description ?? _description,
  rate: rate ?? _rate,
  image: image ?? _image,
);
  String? get title => _title;
  String? get description => _description;
  int? get rate => _rate;
  String? get image => _image;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['title'] = _title;
    map['description'] = _description;
    map['rate'] = _rate;
    map['image'] = _image;
    return map;
  }

}
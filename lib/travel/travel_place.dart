import 'package:bootcamp/travel/travel_model.dart';
import 'package:flutter/material.dart';

class TravelPlace extends StatelessWidget {
  final TravelModel travelList;

  const TravelPlace({Key? key, required this.travelList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.all(8),
        child: ListView.builder(

            itemCount: travelList.places?.length ?? 0,
            itemBuilder: (context, index) {
              Places place = travelList.places?.elementAt(index) ?? Places();
              return Row(
                children: [
                  FadeInImage(
                      fit: BoxFit.cover,
                      width: 150,
                      image: NetworkImage(place.image ?? ''),
                      placeholder: Image.asset(
                        'assets/images/bg_app.png',
                        width: 80,
                        height: 80,
                      ).image),
                  Padding(padding: EdgeInsets.all(8),child: Text(place.title ?? ''))
                ],
              );
            }));
  }
}
